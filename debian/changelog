ruby-kaminari (1.2.2-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Team upload.
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files out of the source package

  [ Caleb Adepitan ]
  * Fix broken refs (tags) match pattern
  * New upstream version 1.2.2

 -- Caleb Adepitan <adepitancaleb@gmail.com>  Thu, 22 Dec 2022 03:50:11 +0000

ruby-kaminari (1.2.1-1) unstable; urgency=medium

  * New upstream version 1.2.1
  * Drop patch applied upstream

 -- Pirate Praveen <praveen@debian.org>  Thu, 02 Jul 2020 01:51:42 +0530

ruby-kaminari (1.0.1-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

  [ Utkarsh Gupta ]
  * Add patch to blacklist "original_script_name" get param.
    (Fixes: CVE-2020-11082) (Closes: #961847)
  * Refresh d/patches
  * Bump debhelper-compat to 13 and Standards-Version to 4.5.0
  * Add myself as an uploader

 -- Utkarsh Gupta <utkarsh@debian.org>  Fri, 05 Jun 2020 09:41:16 +0530

ruby-kaminari (1.0.1-5) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Antonio Terceiro ]
  * Drop explicit debian/tests/control, and let autodep8 handle tests
    (Closes: #950228)
  * Add debian/patches/0001-gemspecs-drop-git-usage.patch (does what it says
    on the tin)
  * Packaging updates from a new run of gem2deb:
    - Bump debhelper compat to 12 and drop debian/compat
    - update homepage from upstream metadata
    - add Rules-Requires-Root: no
    - use ${ruby:Depends} for dependencies
    - Bump Standards-Version to 4.4.0 (no other changes needed)
    - Drop ${shlibs:Depends} as it is pointless for a arch-independent package
  * Add ruby-actionview and ruby-activerecord to Build-Depends:

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 03 Feb 2020 12:14:56 +0100

ruby-kaminari (1.0.1-4) unstable; urgency=medium

  * Add nocheck build profile for backporting

 -- Pirate Praveen <praveen@debian.org>  Thu, 17 May 2018 18:04:01 +0530

ruby-kaminari (1.0.1-3) unstable; urgency=medium

  * Reupload to unstable
  * Bump standards version to 4.1.3 and debhelper compat to 11

 -- Pirate Praveen <praveen@debian.org>  Thu, 22 Feb 2018 13:44:32 +0530

ruby-kaminari (1.0.1-2) experimental; urgency=medium

  * Update dependencies

 -- Pirate Praveen <praveen@debian.org>  Tue, 12 Sep 2017 19:22:36 +0530

ruby-kaminari (1.0.1-1) experimental; urgency=medium

  * New upstream release
  * Add new binary packages: ruby-kaminari-core, ruby-kaminari-actionview,
    ruby-kaminari-activerecord

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Sep 2017 16:47:48 +0530

ruby-kaminari (0.17.0-3) unstable; urgency=medium

  * debian/rules: ship both app/ and config/ using the rubygems layout.
    (Closes:#841487)

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 21 Oct 2016 15:01:18 +0800

ruby-kaminari (0.17.0-2) unstable; urgency=medium

  * Reupload to unstable
  * Check gemspec dependencies during build

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Jul 2016 00:22:27 +0530

ruby-kaminari (0.17.0-1) experimental; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 3.9.8

 -- Abhijith PA <abhijith@openmailbox.org>  Sun, 12 Jun 2016 13:24:54 +0530

ruby-kaminari (0.16.3-1) unstable; urgency=medium

  * New upstream release
  * Fix formatting of bullets, thanks to Daniele Forsi (Closes: #779800)
  * Bump standards version to 3.9.6 (no changes)

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 May 2015 09:38:54 +0530

ruby-kaminari (0.16.1-1) unstable; urgency=low

  * Initial release (Closes: #761030)

 -- Pirate Praveen <praveen@debian.org>  Wed, 10 Sep 2014 16:11:37 +0530
